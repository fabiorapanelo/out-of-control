var _ = require('lodash');
var express = require('express');
var Person = require('../models/Person');

var router = express.Router();

// Simulates a in-memory database of people.
var people = [ new Person(1, 'Alan Ghelardi'),
		new Person(2, 'Christian Wong'),
		new Person(3, 'Daniel Albuquerque'),
		new Person(4, 'Fábio Rapanelo'),
		new Person(5, 'Wellington Macedo') ];

router.get('/people', function(ignore, response) {
	response.json(people);
});

router.get('/people/:id', function(request, response) {
	var id = parseInt(request.params.id);

	var person = _.find(people, {
		id : id
	});

	if (!person) {
		response.sendStatus(404);
	} else {
		response.json(person);
	}
});

module.exports = router;
