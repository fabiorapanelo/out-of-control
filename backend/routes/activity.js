var express = require('express');
var Activity = require('../models/Activity');

var router = express.Router();

router.route('/activities')

// create an activity (accessed at POST
// http://localhost:8080/activities)
.post(function(req, res) {
	var activity = new Activity(); // create a new instance of the
	// Activity model
	activity.name = req.body.name; // set the name (comes from the request)

	// save the activity and check for errors
	activity.save(function(err) {

		if (err) {
			res.send(err);
		}

		res.json({
			message : 'Activity created!'
		});
	});

})

.get(function(req, res) {
	Activity.find(function(err, activities) {
		if (err) {
			res.send(err);
		}
		res.json(activities);
	});
});

router.route('/activities/:activity_id')

// get the activity_id with that id (accessed at GET
// http://localhost:8080/activities/:activity_id)
.get(function(req, res) {
	Activity.findById(req.params.activity_id, function(err, activity) {
		if (err) {
			res.send(err);
		}

		res.json(activity);
	});
})

// update the activity with this id (accessed at PUT
// http://localhost:8080/activities/:activity_id)
.put(function(req, res) {

	Activity.findById(req.params.activity_id, function(err, activity) {

		if (err) {
			res.send(err);
		}

		activity.name = req.body.name;

		// save the activity
		activity.save(function(err) {
			if (err) {
				res.send(err);
			}

			res.json({
				message : 'Activity updated!'
			});
		});

	});
})
// delete the activity with this id (accessed at DELETE
// http://localhost:8080//activities/:activity_id)
.delete(function(req, res) {
	Activity.remove({
		_id : req.params.activity_id
	}, function(err) {
		if (err) {
			res.send(err);
		}

		res.json({
			message : 'Successfully deleted'
		});
	});
});

module.exports = router;