var express = require('express');
var ActionMap = require('../models/ActionMap');

var router = express.Router();

router.route('/action_map')

.post(function(req, res) {
	var action_map = new ActionMap();
	
	action_map.alias = req.body.alias;
	action_map.url = req.body.url;
	action_map.keys = req.body.keys;

	action_map.save(function(err) {

		if (err) {
			res.send(err);
		}

		res.json({
			message : 'Action Map created!'
		});
	});

})

.get(function(req, res) {
	ActionMap.find(function(err, actions_map) {
		if (err) {
			res.send(err);
		}
		res.json(actions_map);
	});
});

router.route('/action_map/:action_map_id')

.get(function(req, res) {
	ActionMap.findById(req.params.action_map_id, function(err, action_map) {
		if (err) {
			res.send(err);
		}

		res.json(action_map);
	});
})

.put(function(req, res) {

	ActionMap.findById(req.params.action_map_id, function(err, action_map) {

		if (err) {
			res.send(err);
		}

		action_map.alias = req.body.alias;
		action_map.url = req.body.url;
		action_map.keys = req.body.keys;

		action_map.save(function(err) {
			if (err) {
				res.send(err);
			}

			res.json({
				message : 'Action Map updated!'
			});
		});

	});
})

.delete(function(req, res) {
	ActionMap.remove({
		_id : req.params.action_map_id
	}, function(err) {
		if (err) {
			res.send(err);
		}

		res.json({
			message : 'Successfully deleted'
		});
	});
});

module.exports = router;