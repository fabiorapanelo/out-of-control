var express = require('express');
var Month = require('../models/Month');
var logger = require('winston');

var router = express.Router();

router.route('/month')

// http://localhost:8080/month)
.post(function(req, res) {
	var month = new Month();
	
	month.name = req.body.name;

	month.save(function(err, model) {

		if (err) {
			res.send(err);
		}

		res.location = '/month/' + model._id;
      	res.sendStatus(201);

	});

})


.get(function(req, res) {

	Month.find().
	limit(10).
	sort('-created').
	exec(function(err, months) {
		if (err) {
			res.send(err);
		}
		
		for (var i = months.length - 1; i >= 0; i--) {
			
			months[i].items = months[i].items.sort(function(a, b) {
	    		return new Date(a.created) - new Date(b.created);
			});
		};

		res.json(months);
	});

});


router.route('/month/:month_id')

.get(function(req, res) {
	Month.findById(req.params.month_id, function(err, month) {
		if (err) {
			res.send(err);
		}

		month.items = month.items.sort(function(a, b) {
    		return new Date(a.created) - new Date(b.created);
		});

		res.json(month);
	});
})

.put(function(req, res) {

	Month.findById(req.params.month_id, function(err, month) {

		if (err) {
			res.send(err);
		}

		month.name = req.body.name;

		month.save(function(err) {
			if (err) {
				res.send(err);
			}

			res.sendStatus(200);

		});

	});
})

.delete(function(req, res) {
	Month.remove({
		_id : req.params.month_id
	}, function(err) {
		if (err) {
			res.send(err);
		}

		res.sendStatus(200);
	});
});


router.route('/month/:month_id/duplicate')

// http://localhost:8080/month)
.post(function(req, res) {

	Month.findById(req.params.month_id, function(err, month) {

		if (err) {
			res.send(err);
		}
		else{

			var duplicated_month = new Month();
			duplicated_month.name = req.body.name;
			
			for (var i = month.items.length - 1; i >= 0; i--) {
				
				delete month.items[i]._id;
				
			};

			duplicated_month.items = month.items;

			duplicated_month.save(function(err) {
				if (err) {
					res.send(err);
				}
				else{
					res.sendStatus(200);	
				}

			});
		}

	});

})

router.route('/month/:month_id/item')
.post(function(req, res){

	Month.findById({"_id": req.params.month_id}, 
	function(err, month){
        if (err) {
			res.send(err);
		}

		if(req.body.item){

			month.items.push(req.body.item);
			month.save();

			res.sendStatus(201);
			logger.info('Item created');

		}else{
			res.sendStatus(400);	
			logger.info('Bad request');
		}
		
    });

});

router.route('/month/:month_id/item/:item_id')
.delete(function(req, res){

	Month.findById({"_id": req.params.month_id}, 
	function(err, month){
        if (err) {
			res.send(err);
		}

		var item = month.items.id(req.params.item_id);
		
		if(item){
			item.remove();
			month.save();

			res.sendStatus(200);
		}
		else{
			res.sendStatus(404);
		}

    });
})
.put(function(req, res){

	Month.findById({"_id": req.params.month_id}, 
	function(err, month){
        if (err) {
			res.send(err);
		}

		var item = month.items.id(req.params.item_id);
		
		if(item){
			
			if(req.body.item.name){
				item.name = req.body.item.name;
			}

			if(req.body.item.planned){
				item.planned = req.body.item.planned;
			}

			if(req.body.item.real){
				item.real = req.body.item.real;
			}

			if(req.body.item.status){
				item.status = req.body.item.status;
			}
			month.save();
			res.sendStatus(200);
		}
		else{
			res.sendStatus(404);
		}
    });

})

.get(function(req, res){
	Month.findById({"_id": req.params.month_id}, 
	function(err, month){
        if (err) {
			res.send(err);
		}
		var item = month.items.id(req.params.item_id);
		
		if(item){
			res.json(item);
		}
		else{
			res.sendStatus(404);
		}
		
    });
});



module.exports = router;