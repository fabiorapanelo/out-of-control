var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var MonthSchema   = new Schema({
    name: String,
    created: { type: Date, default: Date.now },
    items: [{ 
    	name: String, 
    	planned: Number, 
    	real: Number, 
    	status: String, 
    	created: { type: Date, default: Date.now }
    }],
});


module.exports = mongoose.model('Month', MonthSchema);