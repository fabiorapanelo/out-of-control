var logger = require('winston');
var mongoose = require('mongoose');

var database = module.exports;

//var url = 'mongodb://5thgrade:5thgrade@ds041154.mongolab.com:41154/5thgrade';
var url = 'mongodb://admin:admin@ds053964.mongolab.com:53964/ooc';

database.configure = function() {
	mongoose.connect(url);
	var db = mongoose.connection;
	db.on('error', function(error) {
		logger.error('Connection error');
		logger.error(error);
	});

	db.once('open', function() {
		logger.info('Database connected');
	});
};
