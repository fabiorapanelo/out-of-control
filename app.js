var bodyParser = require('body-parser');
var express = require('express');
var logger = require('winston');
//var cors = require('cors')

var database = require('./backend/infrastructure/database');
//var people = require('./routes/people');
//var activity = require('./routes/activity');
var month = require('./backend/routes/month')
//var default_router = require('./backend/routes/default')


// ---------------------------------------------------
// Configuring the express
var port = process.env.PORT || 8080;
var app = express();

//Enable CORS
//app.use(cors());

app.use(bodyParser.urlencoded({
	extended : true
}));

app.use(bodyParser.json());


//app.use(people);
//app.use(activity);
app.use(month);
//app.use(default_router);

//Serving static files
app.use(express.static('frontend'));

app.listen(port, function() {
	database.configure();
	logger.info('The REST application is ready and is listening at port %s', port);
});