require([
	"jquery", 
	"knockout", 
	"komapping", 
	"underscore", 
	"RESTModel",
	"chart",
	'bootstrap'], 
	function($, ko, mapping, _ , RESTModel, Chart){  

$(function () {

	// Here's my data model
	var ViewModel = function() {
	    
	    var self = this;

	    self.numberRequests = ko.observable(0);
	    self.RESTModel = new RESTModel('/month', self.numberRequests);

	    self.months = ko.observableArray([]);
	    self.currentMonthId = ko.observable(null);
	    self.currentMonthItems = mapping.fromJS([]);
	    self.selectedItem = ko.observable(null);

	    self.confirmationMessage = ko.observable(false);
	    self.chartContext = document.getElementById("myChart").getContext("2d");
	    self.doughnutChart = null;

	    self.emptyPage = ko.computed(function() {
	    	//If one of the following is populated, then page is not empty
	        return !(this.currentMonthId() || this.selectedItem());
	    }, self);

	    self.displayMonth = ko.computed(function(){

	    	return this.currentMonthId() && !this.selectedItem();

	    }, self);

	    self.totalCounter = ko.computed(function(){

	    	return self.currentMonthItems().length + (self.currentMonthItems().length == 1 ?' element':' elements');
	    
	    }, self);

	    self.totalReal = ko.computed(function(){

	    	var total = 0;
	    	_.each(self.currentMonthItems(), function(element){
	    		total = total + parseInt(element.real());
	    	});
	    	return "Total is  " + total;

	    }, self);

	    self.totalPlanned = ko.computed(function(){

	    	var total = 0;
	    	_.each(self.currentMonthItems(), function(element){
	    		total = total + parseInt(element.planned());
	    	});
	    	return "Total is  " + total;

	    }, self);

	    self.finalStatus = ko.computed(function(){

	    	var finalStatus = "OK";
	    	_.each(self.currentMonthItems(), function(element){

	    		if(element.status() == "N.OK"){
	    			finalStatus = "N.OK";
	    		}
	    	});

	    	return finalStatus;

	    }, self);

	    self.generateChartData = ko.computed(function(){

	    	if(self.doughnutChart){
	    		self.doughnutChart.destroy();
	    	}

	    	var chartData = [];

		    _.each(self.currentMonthItems(), function(element){

	    		chartData.push({
	        		value: element.planned(),
	        		color: '#337AB7',
	        		highlight: '#265A88',
	        		label: element.name()
	    		});

	    	});

	    	self.doughnutChart = new Chart(self.chartContext).Doughnut(chartData);

	    }, self);

	    self.dynamicColors = function(){
	    	
		    var r = Math.floor(Math.random() * 255);
		    var g = Math.floor(Math.random() * 255);
		    var b = Math.floor(Math.random() * 255);
		    return "rgb(" + r + "," + g + "," + b + ")";
	    }

	    self._retrieveMonths = function(){

	    	self.RESTModel.invoke('GET', null, null, function(data){
	    		self.months(data);
	    	});
	    }

	    self.setCurrentMonth = function(month){

	    	self.confirmationMessage(false);
	    	self.currentMonthId(month._id);
	    	//self.currentMonthItems(month.items);
	    	mapping.fromJS(month.items, self.currentMonthItems);

	    	self.selectedItem(null);
	    }

	    self.createMonth = function(test, event){
	    	var month = prompt("Please enter month and year", "");
			if (month != null) {
				var data = {
					name: month
				}

			    self.RESTModel.invoke('POST', null, data, function(){
			    	
			    	self.confirmationMessage(true);
			    	self._retrieveMonths();

			    });
			}
	    }

	    self.duplicateMonth = function(test, event){
	    	var month = prompt("Please enter month and year", "");
			if (month != null) {
				
				var pathParams = { 
		    		month_id: self.currentMonthId()
		    	}

				var data = {
					name: month
				}

				self.RESTModel.invoke('POST', pathParams, data, function(data){

		    		self.confirmationMessage(true);
			    	self._retrieveMonths();

		    	}, '/month/:month_id/duplicate');
			}
	    }

	    self.deleteMonth = function(){

	    	var sure = confirm("Are you sure?");
			if (sure == true) {
			
		    	var pathParams = { 
		    		month_id: self.currentMonthId()
		    	}

		    	self.RESTModel.invoke('DELETE', pathParams, null, function(){
		    		self.confirmationMessage(true);
		    		self._retrieveMonths();
		    		self.currentMonthId(null);
		    	}, '/month/:month_id');

			}
	    }

	    self.selectItem = function(item){
	    	self.confirmationMessage(false);
	    	self.selectedItem(item);
	    	$("input[name='name']").val(self.selectedItem().name());
    		$("input[name='planned']").val(self.selectedItem().planned());
    		$("input[name='real']").val(self.selectedItem().real());
	    }

	    self.deleteItem = function(item){

	    	var sure = confirm("Are you sure?");
			if (sure == true) {
			
		    	var pathParams = {
		    		month_id: self.currentMonthId(),
		    		item_id: item._id()
		    	}

		    	self.RESTModel.invoke('DELETE', pathParams, null, function(data){

		    		self.confirmationMessage(true);
		    		self.selectedItem(null);
		    		self._retrieveMonthItems();

		    	}, '/month/:month_id/item/:item_id');
		    }
	    }

	    self.cancelCreation = function(item){
	    	self.selectedItem(null);
	    }

	    self.selectEmptyItem = function(){
	    	
	    	var emptyItem = mapping.fromJS({
	    		_id: 0,
	    		name: '',
	    		planned: 0,
	    		real: 0,
	    		status: 'N.OK'
	    	});
	    	self.selectItem(emptyItem);
	    }

	    self.saveItem = function(){
    	
    		
    		self.selectedItem().name($("input[name='name']").val());
    		self.selectedItem().planned($("input[name='planned']").val());
    		self.selectedItem().real($("input[name='real']").val());

	    	//Set status
    		var status = 'N.OK';
	    	if(self.selectedItem().planned() == self.selectedItem().real()){
	    		status = 'OK';
	    	}
	    	self.selectedItem().status(status);

	    	if(self.selectedItem()._id()){

	    		var pathParams = {
		    		month_id: self.currentMonthId(),
					item_id: self.selectedItem()._id()		    		
		    	}

		    	var existingItem = {
		    		"item": {
		    			"_id": self.selectedItem()._id(),
		    			"name": self.selectedItem().name(),
		    			"planned": self.selectedItem().planned(),
		    			"real": self.selectedItem().real(),
		    			"status": self.selectedItem().status()
		    		}
		    	}

		    	self.RESTModel.invoke('PUT', pathParams, existingItem, function(data){

		    		self.confirmationMessage(true);
		    		self.selectedItem(null);
		    		//self._retrieveMonthItems();

		    	}, '/month/:month_id/item/:item_id');

	    	}else{

	    		var pathParams = {
		    		month_id: self.currentMonthId()
		    	}

		    	var newItem = {
		    		"item": {
		    			"name": self.selectedItem().name,
		    			"planned": self.selectedItem().planned,
		    			"real": self.selectedItem().real,
		    			"status": self.selectedItem().status()
		    		}
		    	}

		    	self.RESTModel.invoke('POST', pathParams, newItem, function(data){

		    		self.confirmationMessage(true);
		    		self.selectedItem(null);
		    		self._retrieveMonthItems();

		    	}, '/month/:month_id/item');

	    	}
	    }

	    self._retrieveMonthItems = function(){
	    	var pathParams = {
	    		month_id: self.currentMonthId()
	    	}

	    	self.RESTModel.invoke('GET', pathParams, null, function(month){

	    		//self.currentMonthItems(month.items);
	    		mapping.fromJS(month.items, self.currentMonthItems);

	    		var selectedMonth = _.find(self.months(), function(m){
		    		return m._id == month._id;
		    	});
		    	selectedMonth.items =  month.items;

	    	}, '/month/:month_id');
	    }

	    self.hideConfirmationMessage = function(){
	    	self.confirmationMessage(false);
	    }
	    

	/*
	var data = [
	    {
	        value: 300,
	        color:"#F7464A",
	        highlight: "#FF5A5E",
	        label: "Red"
	    },
	    {
	        value: 50,
	        color: "#46BFBD",
	        highlight: "#5AD3D1",
	        label: "Green"
	    },
	    {
	        value: 100,
	        color: "#FDB45C",
	        highlight: "#FFC870",
	        label: "Yellow"
	    }
	]
	*/
	

	    self._retrieveMonths();

	}
	 
	ko.applyBindings(new ViewModel()); // This makes Knockout get to work

});

});