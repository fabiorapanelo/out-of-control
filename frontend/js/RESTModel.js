define(['jquery', "komapping"], function ($, ko) {

    return function (url, numberRequests){
		var self = this;
		self.url = url;
		self.numberRequests = numberRequests;

		self.invoke = function (httpMethod, pathParams, data, callback, url, debug){

			//url is not mandatory when you invoke the function
			var finalUrl = typeof url !== 'undefined' ? url : self.url;

			for(var param in pathParams){
				finalUrl = finalUrl.replace(':'+param, pathParams[param])
			}

			//debug = true;

			if(debug){
				console.log('FinalURL: '+ finalUrl);
				console.log('httpMethod: '+ httpMethod);
				console.log('pathParams: '+ ko.toJSON(pathParams));
				console.log('data: '+ ko.toJSON(data));
			}
			
			self.numberRequests(self.numberRequests() + 1);

			$.ajax({
				method: httpMethod,
				url: finalUrl,
				data: data,
				cache: false,
				async: false
			}).done(callback).fail(function( jqXHR, textStatus ) {

				alert( "Request failed: " + textStatus );
							
			}).always(function(){
				self.numberRequests(self.numberRequests() - 1);
			});
		}
	}
});

