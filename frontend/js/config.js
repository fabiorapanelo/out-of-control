require.config({
    baseUrl: "/js",
    paths: {
        'jquery': 'lib/jquery-2.1.4.min',
        'knockout': 'lib/knockout-min',
        'komapping': 'lib/knockout.mapping.min',
        'underscore': 'lib/underscore-min',
        'bootstrap': 'lib/bootstrap.min',
        'chart': 'lib/Chart'
    },
    shim: {
        "bootstrap": {
            deps: ["jquery"]
        }
    }
  });